# 전체 소스, WAR 파일, DB스키마(엑셀), 화면캡쳐이미지

## :book: 목차

### 1. [전체 소스](#)

- back > [https://gitlab.com/jinjinjin/tisis_subject2/-/tree/master/back/demo](https://gitlab.com/jinjinjin/tisis_subject2/-/tree/master/back/demo)
- front > [https://gitlab.com/jinjinjin/tisis_subject2/-/tree/master/front/movie](https://gitlab.com/jinjinjin/tisis_subject2/-/tree/master/front/movie)

### 2. [WAR 파일]

- war > [https://gitlab.com/jinjinjin/tisis_subject2/-/blob/master/demo-0.0.1-SNAPSHOT.war](https://gitlab.com/jinjinjin/tisis_subject2/-/blob/master/demo-0.0.1-SNAPSHOT.war)

### 3. [DB스키마(엑셀)]

- DB스키마 > https://gitlab.com/jinjinjin/tisis_subject2/-/tree/master/DB

- ERD

![ERD](DB/erd.PNG)


### 4. [화면캡쳐이미지]
---
* 메인페이지


 ![메인페이지](capture/mainpage.PNG)
---
* 영화 선택페이지 (전체 상영표)


![영화 선택페이지 (전체 상영표)](capture/movieAllinfoPage.PNG)
---
* 영화 선택페이지 (특정 영화 선택 상영표)


![영화 선택페이지 (특정 영화 선택 상영표)](capture/selectedMovieInfoPage.PNG)
---
* 예매 페이지


![예매 페이지](capture/BookPage.PNG)
---
* 예매 확인페이지 (핸드폰 번호로 조회시)


![예매 확인페이지 (핸드폰 번호로 조회시)](capture/BookCheckedPage.PNG)
