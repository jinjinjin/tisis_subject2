import Vue from 'vue'
import App from './App.vue'
import vuetify from './plugins/vuetify';

// store.js를 불러오는 코드
import store from './store';  
import router from './router'; 
  

new Vue({
  store,
  vuetify,
  router,
  render: (h) => h(App)
}).$mount("#app");
