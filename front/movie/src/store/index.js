// store.js
import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
    //예시 데이터
    state: {
        counter: 0
      }
});
