import Vue from "vue";
import VueRouter from "vue-router";

//main page
import MainPage from "../components/main/MainPage";
//page
import BookPage from "../components/main/BookPage";

//table component
import SeatTable from "../components/table/SeatTable"
import BookCheckTable from "../components/table/BookCheckTable"
//import BookTable from "../components/table/BookTable";
//import CheckBookTable from "../components/table/CheckBookTable";

Vue.use(VueRouter);
const routes = [
  { path: "/", name: "MainPage", component: MainPage },
  {
    path: "/book",
    name: "BookPage",
    component: BookPage, 
  },
  { path: "/seattable", name: "SeatTable", component: SeatTable },
  { path: "/bookchecktable", name: "BookCheckTable", component: BookCheckTable },
 // { path: "/booktable", name: "BookTable", component: BookTable },
 // { path: "/checkbooktable", name: "CheckBookTable", component: CheckBookTable },
];
const router = new VueRouter({
  mode: "history",
  routes,
});

export default router;
