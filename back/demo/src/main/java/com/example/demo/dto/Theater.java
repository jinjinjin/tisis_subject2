package com.example.demo.dto;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
 
@Data
public class Theater {
	
	private Integer theater_id; 
	private Integer movie_id; 
	private Integer seat_col_num;
	private Integer seat_row_num;
	private String theater_name;
	
 
	

}
