package com.example.demo.dao;
import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import com.example.demo.dto.Movie; 

@Repository
@Mapper
public interface MovieDAO {

	public Movie getMovieInfo(int movie_id);
	public List<Movie> selectAllMovie();
}
