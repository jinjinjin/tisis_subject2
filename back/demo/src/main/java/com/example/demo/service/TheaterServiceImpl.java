package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.dao.TheaterDAO;
import com.example.demo.dto.Theater;

@Service
public class TheaterServiceImpl implements TheaterService {

	@Autowired
	TheaterDAO theaterDAO;
	
	@Override
	public List<Theater> selectAll() {
		// TODO Auto-generated method stub
		return theaterDAO.selectAll();
	}

	@Override
	public List<Theater> selectByMovieId(Integer movie_id) {
		// TODO Auto-generated method stub
		return theaterDAO.selectByMovieId(movie_id);
	}
 

}
