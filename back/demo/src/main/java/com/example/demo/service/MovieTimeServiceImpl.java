package com.example.demo.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.dao.MovieTimeDAO;

@Service
public class MovieTimeServiceImpl implements MovieTimeService {

	@Autowired
	MovieTimeDAO movieTimeDAO;

	@Override
	public List<String> getAllMovieTimeByIds(int theater_id, int movie_id) {
		// TODO Auto-generated method stub
		Map<String, Integer> paramMap = new HashMap<String, Integer>();
		paramMap.put("theater_id", theater_id);
		paramMap.put("movie_id", movie_id);
		return movieTimeDAO.getAllMovieTimeByIds(paramMap);
	}

	@Override
	public String getMovieTimeId(String theater_id, String movie_id, String movie_time) {
		// TODO Auto-generated method stub
		Map<String, String> paramMap = new HashMap<String, String>();
		paramMap.put("theater_id", theater_id);
		paramMap.put("movie_id", movie_id);
		paramMap.put("movie_time", movie_time);
		return movieTimeDAO.getMovieTimeId(paramMap);
	}
}
