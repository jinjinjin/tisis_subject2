package com.example.demo.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;
@Repository
@Mapper
public interface MovieTimeDAO {
	
	public List<String> getAllMovieTimeByIds(Map paramMap);
	public String getMovieTimeId(Map paramMap);
}
