package com.example.demo.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import com.example.demo.dto.Theater;
 
@Repository
@Mapper
public interface TheaterDAO {

	public List<Theater> selectAll(); 
	public List<Theater> selectByMovieId(Integer movie_id); 
}
