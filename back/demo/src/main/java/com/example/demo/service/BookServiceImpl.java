package com.example.demo.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.dao.BookDAO;
import com.example.demo.dto.Book;

@Service
public class BookServiceImpl implements BookService {

	@Autowired
	BookDAO bookDAO;
	
	@Override
	public List<Book> searchAllByPhone(String phone_num) {
		// TODO Auto-generated method stub
		return bookDAO.searchAllByPhone(phone_num);
	}

	@Override
	public boolean insertBook(Book book) {
		// TODO Auto-generated method stub
		return bookDAO.insertBook(book) == 1;
	}

	@Override
	public List<String> selectAllSeats(String theater_id, String movie_id, String movie_time_id) {
		// TODO Auto-generated method stub
		Map<String, String> paramMap = new HashMap<String, String>();
		paramMap.put("theater_id", theater_id);
		paramMap.put("movie_id", movie_id);
		paramMap.put("movie_time_id", movie_time_id);
		return bookDAO.selectAllSeats(paramMap);
	}

}
