package com.example.demo.dto;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

 
@Data
public class MovieTime {
	private Integer theater_id;
	private Integer movie_id;
	private Integer movie_time_id;
	private String movie_time;
	
	 
	
}
