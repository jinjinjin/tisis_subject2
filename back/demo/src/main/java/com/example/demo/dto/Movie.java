package com.example.demo.dto;
 

import lombok.Data; 
 
@Data
public class Movie {

	private Integer movie_id;
	private String movie_name;
	private Integer movie_price; 
	private String movie_img; 

 

}
