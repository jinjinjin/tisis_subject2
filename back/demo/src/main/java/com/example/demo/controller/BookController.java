package com.example.demo.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dto.Book;
import com.example.demo.service.BookServiceImpl;
import com.example.demo.service.MovieTimeServiceImpl;

import io.swagger.annotations.ApiOperation;

@CrossOrigin(origins = { "*" }, maxAge = 6000)
@RestController
@RequestMapping("/book")
public class BookController {

	private static final String SUCCESS = "success";
	private static final String FAIL = "fail";

	@Autowired
	BookServiceImpl bookServiceImpl;
	
	@Autowired
	MovieTimeServiceImpl movieTimeServiceImpl;

 
	@ApiOperation(value = "핸드폰 번호에 해당하는 예매정보를 반환한다.", response = Book.class)
	@GetMapping("/phonenum/{phone_num}")
	public ResponseEntity<List<Book>> searchAllByPhone(@PathVariable String phone_num) {

		return new ResponseEntity<List<Book>>(bookServiceImpl.searchAllByPhone(phone_num), HttpStatus.OK);
	}

	@ApiOperation(value = "예매 정보를 입력한다. 그리고 DB입력 성공여부에 따라 'success' 또는 'fail' 문자열을 반환한다.", response = String.class)
	@PostMapping("/insert")
	public ResponseEntity<String> insertBook(@RequestParam(required = true)  String theater_id,
			@RequestParam(required = true)  String movie_id,
			@RequestParam(required = true)  String phone_num,
			@RequestParam(required = true)  String seat_name,
			@RequestParam(required = false)  String movie_time) {

		ArrayList<String> seats = new ArrayList();
		StringTokenizer st = new StringTokenizer(seat_name, ","); 
		while(st.hasMoreTokens()) {
			seats.add(st.nextToken());
		}
		 
		String movietimeid = movieTimeServiceImpl.getMovieTimeId(theater_id, movie_id, movie_time);
		for(int i = 0; i < seats.size(); i++) {
			Book book = new Book();
			book.setTheater_id(Integer.parseInt(theater_id));
			book.setMovie_id(Integer.parseInt(movie_id));
			book.setMovie_time_id(Integer.parseInt(movietimeid));
			book.setPhone_num(phone_num);
			book.setSeat_name(seats.get(i));
			
			if(!bookServiceImpl.insertBook(book)) {
				return new ResponseEntity<String>(FAIL, HttpStatus.CREATED);
			}
		} 
		return new ResponseEntity<String>(SUCCESS, HttpStatus.CREATED);
	}

	@ApiOperation(value = "예매된 모든 좌석 정보를 반환한다.", response = Book.class)
	@GetMapping("/seats")
	public ResponseEntity<List<String>> selectAllSeats(@RequestParam(required = true)  String theater_id,
			@RequestParam(required = true)  String movie_id,
			@RequestParam(required = true)  String movie_time) {
		
		String movietimeid = movieTimeServiceImpl.getMovieTimeId(theater_id, movie_id, movie_time);
		
		//String theater_id, String movie_id, String movie_time_id
		return new ResponseEntity<List<String>>(bookServiceImpl.selectAllSeats(theater_id, movie_id, movietimeid), HttpStatus.OK);
	}

}
