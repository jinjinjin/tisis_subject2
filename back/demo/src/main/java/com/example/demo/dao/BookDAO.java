package com.example.demo.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import com.example.demo.dto.Book; 
@Repository
@Mapper
public interface BookDAO {

	public List<Book> searchAllByPhone(String phone_num);
	public int insertBook(Book book);
	public List<String> selectAllSeats(Map paramMap);
}
