package com.example.demo.service;

import java.util.List;

import com.example.demo.dto.Movie;

public interface MovieService {
	public Movie getMovieInfo(int movie_id);
	public List<Movie> selectAllMovie();
}
