package com.example.demo.service;

import java.util.List;
 
import com.example.demo.dto.Theater;

public interface TheaterService {
	
	 
	
	public List<Theater> selectAll(); 
	public List<Theater> selectByMovieId(Integer movie_id); 
}
