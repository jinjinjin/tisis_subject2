package com.example.demo.service;

import java.util.List;

import com.example.demo.dto.Book;

public interface BookService {
	public List<Book> searchAllByPhone(String phone_num);
	public boolean insertBook(Book book);
	public List<String> selectAllSeats(String theater_id, String movie_id, String movie_time_id);
}
