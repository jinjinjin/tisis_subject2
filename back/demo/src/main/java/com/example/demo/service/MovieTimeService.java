package com.example.demo.service;

import java.util.List;
import java.util.Map; 

public interface MovieTimeService {
	public List<String> getAllMovieTimeByIds(int theater_id, int movie_id);
	public String getMovieTimeId(String theater_id, String movie_id, String movie_time);
}
