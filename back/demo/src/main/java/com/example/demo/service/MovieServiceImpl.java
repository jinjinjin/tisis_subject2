package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service; 

import com.example.demo.dao.MovieDAO;
import com.example.demo.dto.Movie;
  
@Service
public class MovieServiceImpl implements MovieService {

	@Autowired
	MovieDAO movieDAO;
	
	
	@Override
	public Movie getMovieInfo(int movie_id) {
		// TODO Auto-generated method stub
		return movieDAO.getMovieInfo(movie_id);
	}


	@Override
	public List<Movie> selectAllMovie() {
		// TODO Auto-generated method stub
		return movieDAO.selectAllMovie();
	}

}
