package com.example.demo.controller;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dto.Book;
import com.example.demo.dto.Movie;
import com.example.demo.dto.Theater;
import com.example.demo.service.MovieServiceImpl;
import com.example.demo.service.MovieTimeServiceImpl;
import com.example.demo.service.TheaterServiceImpl;

import io.swagger.annotations.ApiOperation;

@CrossOrigin(origins = { "*" }, maxAge = 6000)
@RestController
@RequestMapping("/movie")
public class MovieController {

	@Autowired
	private MovieServiceImpl movieServiceImpl;

	@Autowired
	private TheaterServiceImpl theaterServiceImpl;

	@Autowired
	private MovieTimeServiceImpl movieTimeServiceImpl;

	@ApiOperation(value = "모든 영화정보를 반환한다.", response = Movie.class)
	@GetMapping("/movieinfos")
	public ResponseEntity<List<Movie>> selectAllSeats() {

		return new ResponseEntity<List<Movie>>(movieServiceImpl.selectAllMovie(), HttpStatus.OK);
	}
 
	@ApiOperation(value = "영화id에 따른 예매가능 극장정보를 반환한다", response = Theater.class) 
	@GetMapping("/theater")
	public ResponseEntity<Map<String, Object>> getAllTheaterInfosByMovieid(@RequestParam(required = false) Integer movieid)
			throws Exception {
		List<Theater> theaters;
		if (movieid == null) {
			theaters = theaterServiceImpl.selectAll();
		}else {
			theaters = theaterServiceImpl.selectByMovieId(movieid);
		} 
		List<String> movieName = new LinkedList();
		List<List<String>> movieTime = new LinkedList();

		Map<String, Object> result = new HashMap();

		Integer movieId = null;

		for (int i = 0; i < theaters.size(); i++) {
			if (movieid == null) {
				movieId = theaters.get(i).getMovie_id();
			}else {
				movieId = movieid;
			}
			Movie movie = movieServiceImpl.getMovieInfo(movieId);
			movieName.add(movie.getMovie_name());
			movieTime.add(
					movieTimeServiceImpl.getAllMovieTimeByIds(theaters.get(i).getTheater_id(), movie.getMovie_id()));
		}
		result.put("theater", theaters);
		result.put("movieName", movieName);
		result.put("movieTime", movieTime);

		return new ResponseEntity<>(result, HttpStatus.OK);

	}

}
