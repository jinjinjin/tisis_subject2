package com.example.demo.dto;

import lombok.Data; 
 
@Data
public class Book {
	private Integer book_id;
	private Integer theater_id; 
	private Integer movie_id; 
	private Integer movie_time_id; 
	private String phone_num; 
	private String seat_name;  
	
	//서비스에서 필요로하는 컬럼들을 얻기위해 조인해야할 테이블
	private Movie movie;
	private MovieTime movieTime;
	private Theater theater;
}
