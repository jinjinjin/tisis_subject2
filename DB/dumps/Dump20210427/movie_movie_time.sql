-- MySQL dump 10.13  Distrib 8.0.23, for Win64 (x86_64)
--
-- Host: localhost    Database: movie
-- ------------------------------------------------------
-- Server version	8.0.23

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `movie_time`
--

DROP TABLE IF EXISTS `movie_time`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `movie_time` (
  `movie_time_id` int NOT NULL AUTO_INCREMENT COMMENT '상영시간아이디',
  `theater_id` int NOT NULL COMMENT '영화관아이디',
  `movie_id` int NOT NULL COMMENT '영화아이디',
  `movie_time` varchar(10) DEFAULT NULL COMMENT '상영시간',
  PRIMARY KEY (`movie_time_id`,`theater_id`,`movie_id`),
  KEY `FK_theater_TO_movie_time` (`theater_id`,`movie_id`),
  KEY `FK_movie_TO_movie_time` (`movie_id`),
  CONSTRAINT `FK_movie_TO_movie_time` FOREIGN KEY (`movie_id`) REFERENCES `movie` (`movie_id`),
  CONSTRAINT `FK_theater_TO_movie_time` FOREIGN KEY (`theater_id`, `movie_id`) REFERENCES `theater` (`theater_id`, `movie_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COMMENT='영화 상영시간 테이블';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `movie_time`
--

LOCK TABLES `movie_time` WRITE;
/*!40000 ALTER TABLE `movie_time` DISABLE KEYS */;
INSERT INTO `movie_time` VALUES (1,1,1,'9:00'),(2,1,1,'15:00'),(3,1,1,'21:00'),(4,2,2,'8:30'),(5,2,2,'13:30'),(6,2,2,'18:30'),(7,3,3,'10:00'),(8,3,3,'14:00'),(9,3,3,'18:00'),(10,3,3,'22:00');
/*!40000 ALTER TABLE `movie_time` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-04-27 15:44:33
