-- MySQL dump 10.13  Distrib 8.0.23, for Win64 (x86_64)
--
-- Host: localhost    Database: movie
-- ------------------------------------------------------
-- Server version	8.0.23

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `book`
--

DROP TABLE IF EXISTS `book`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `book` (
  `book_id` int NOT NULL AUTO_INCREMENT COMMENT '예약번호',
  `theater_id` int NOT NULL COMMENT '영화관아이디',
  `movie_id` int NOT NULL COMMENT '영화아이디',
  `movie_time_id` int NOT NULL COMMENT '상영시간아이디',
  `phone_num` varchar(20) DEFAULT NULL COMMENT '핸드폰번호',
  `seat_name` varchar(10) DEFAULT NULL COMMENT '좌석이름',
  PRIMARY KEY (`book_id`,`theater_id`,`movie_id`,`movie_time_id`),
  KEY `FK_theater_TO_book` (`theater_id`,`movie_id`),
  KEY `FK_movie_TO_book` (`movie_id`),
  KEY `FK_movie_time_TO_book` (`movie_time_id`,`theater_id`,`movie_id`),
  CONSTRAINT `FK_movie_time_TO_book` FOREIGN KEY (`movie_time_id`, `theater_id`, `movie_id`) REFERENCES `movie_time` (`movie_time_id`, `theater_id`, `movie_id`),
  CONSTRAINT `FK_movie_TO_book` FOREIGN KEY (`movie_id`) REFERENCES `movie` (`movie_id`),
  CONSTRAINT `FK_theater_TO_book` FOREIGN KEY (`theater_id`, `movie_id`) REFERENCES `theater` (`theater_id`, `movie_id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8 COMMENT='예약 테이블';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `book`
--

LOCK TABLES `book` WRITE;
/*!40000 ALTER TABLE `book` DISABLE KEYS */;
INSERT INTO `book` VALUES (1,1,1,1,'1','A1'),(2,1,1,1,'1','B1'),(3,1,1,1,'1','B3'),(4,1,1,1,'1','A2'),(5,1,1,1,'1','A3'),(6,3,3,9,'123123','I6'),(7,3,3,9,'123123','H7'),(8,1,1,1,'123','C7'),(9,1,1,1,'123','E8'),(10,1,1,1,'123','H5'),(11,1,1,1,'123','F4'),(12,1,1,1,'123','J7'),(13,1,1,1,'123123','A11'),(14,1,1,1,'123123','A12'),(15,1,1,1,'123123','B12'),(16,1,1,1,'123123','B11'),(17,2,2,6,'123123','F5'),(18,2,2,6,'123123','G4'),(19,1,1,1,'123123','M12'),(20,1,1,1,'123123','K12');
/*!40000 ALTER TABLE `book` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-04-27 15:44:33
